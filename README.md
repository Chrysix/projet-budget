
# Projet Budget


Je vous présente le projet que j'ai eu à faire pendant ma formation chez Simplon pour développer mes compétence en React et en Node/Express.

Le but était de créer une application de gestion de budget avec un back et un front.

Elle permettra d'avoir accès à ses différentes opérations de dépenses, ajouter des opérations ou bien les supprimés, ainsi qu'un affichage du total de ces opérations et une option permettant de voir ces dépenses d'un mois spécifique.


### Projet: 

https://projet-front-budget.herokuapp.com/


### Git du back:

https://gitlab.com/Chrysix/projet-budget

### Git du front:

https://gitlab.com/Chrysix/front-budget

### Diagramme de classe

https://app.diagrams.net/#LDiagrame%20de%20classe%20projet_budget
