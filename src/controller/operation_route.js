import { Router } from "express";
import { RepositoryOperation } from "../repository/RepositoryOperation";

export const operation_route = Router();

//Montrer toute les opérations
operation_route.get("/", async (req, resp) => {
    const operations = await RepositoryOperation.findAll()
    resp.json(operations)
});

//Montrer les opérations possedant une category defini
operation_route.get('/category/:category', async (req, resp) => {
    let operations = await RepositoryOperation.findCategory(req.params.category)
    resp.json(operations)
})

//Montrer les opérations avec id defini
operation_route.get('/:id', async (req, resp) => {
    let operations = await RepositoryOperation.findId(req.params.id)
    resp.json(operations)
})

//Montrer les opérations avec mois defini
operation_route.get('/date/:date', async (req, resp) => {
    let operations = await RepositoryOperation.findMonth(req.params.date)
    resp.json(operations)
})

//Ajouter une opération
operation_route.post("/", async (req, resp) => {

    try {
        await RepositoryOperation.add(req.body)
        resp.status(201).json(req.body)
    } catch (error) {
        console.log(error);
        resp.status(500).json(error)
    }

});

//Supprimer une opération
operation_route.delete('/:id', async (req, resp) => {
    await RepositoryOperation.delete(req.params.id)
    resp.end()
})

//Mettre à jour une opération
operation_route.put("/", async (req, resp) => {
    await RepositoryOperation.update(req.body)
    resp.end()
});

operation_route.patch('/:id', async (req, res) => {
    try {
        let operations = await RepositoryOperation.findId(req.params.id);
        if (!operations) {
            res.send(404).end();
            return
        }
        let update = { ...operations, ...req.body };
        await RepositoryOperation.update(update);
        res.json(update);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
});
