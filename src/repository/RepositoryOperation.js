import { connection } from "./connection";
import { Operation } from "../entity/operation";

export class RepositoryOperation {

    //Montrer toute les opérations
    static async findAll() {
        const [rows] = await connection.query(`SELECT * FROM operation`);
        let operations = []
        for (const row of rows) {
            let instance = new Operation(row.label, row.price, row.category, row.date, row.id);
            operations.push(instance);
        }
        return operations

    };


    //Montrer les opérations possedant une category defini
    static async findCategory(category) {
        const [rows] = await connection.query(`SELECT * FROM operation WHERE category=?`, [category]);
        if (rows.length === 1) {
            let instance = new Operation(rows[0].label, rows[0].price, rows[0].category, rows[0].date, rows[0].id);
            return instance
        }
        return null;
    };


    //Montrer les opérations avec id defini
    static async findId(id) {
        const [rows] = await connection.query(`SELECT * FROM operation WHERE id=?`, [id]);
        if (rows.length === 1) {
            let instance = new Operation(rows[0].label, rows[0].price, rows[0].category, rows[0].date, rows[0].id);
            return instance
        }
        return null;
    };

    //Montrer les opérations par mois defini
    static async findMonth(month) {
        let [rows] = await connection.query(`SELECT * FROM operation WHERE MONTH(date)=?`, [month]);
        let operations = [];
        for (const row of rows) {
            let instance = new Operation(row.label, row.price, row.category, row.date, row.id);
            operations.push(instance);
        }
        return operations;
    }

    //Ajouter une opérations
    static async add(op) {
        const [rows] = await connection.query('INSERT INTO operation (label, price, category, date) VALUES (?, ?, ?, ?)', [op.label, op.price, op.category, op.date])
        op.id = rows.insertId;
    };

    //Supprimer une opérations
    static async delete(id) {
        await connection.query("DELETE FROM operation WHERE id= ?", [id])
    };


    static async update(upOp) {
        await connection.query('UPDATE operation SET label = ?, price = ?, category = ?, date = ?  WHERE id = ?', [upOp.label, upOp.price, upOp.category, upOp.date, upOp.id])
    };
};

