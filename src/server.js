import express from "express";
import cors from "cors";
import { operation_route } from "./controller/operation_route";

export const server = express()


server.use(express.json());
server.use(cors());

server.use('/api/operation', operation_route)