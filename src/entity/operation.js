
export class Operation{
    id;
    label;
    price;
    category;
    date;

    constructor(label, price, category, date, id){
        this.label = label;
        this.price = price;
        this.category = category;
        this.date = date;
        this.id = id;
    }
}