USE appbudget;


DROP TABLE IF EXISTS `operation`;

CREATE TABLE `operation` (
    `id` INT AUTO_INCREMENT PRIMARY KEY,
    `label` VARCHAR (256) DEFAULT NULL,
    `price` FLOAT (20) DEFAULT NULL,
    `category`VARCHAR (256) DEFAULT NULL,
    `date` DATE DEFAULT NULL
);

INSERT INTO `operation` (`label`, `price`, `category`, `date`) VALUES ('Tamtamerie', 50, 'Restauration', '2021-06-14');

INSERT INTO `operation` (`label`, `price`, `category`, `date`) VALUES ('Maison A3', 530, 'Logement', '2021-05-24');

INSERT INTO `operation` (`label`, `price`, `category`, `date`) VALUES ('E.Leclerc', 95, 'Alimentation', '2021-05-02');

INSERT INTO `operation` (`label`, `price`, `category`, `date`) VALUES ('Micromania', 46, 'Achat & Shopping', '2021-03-19');

INSERT INTO `operation` (`label`, `price`, `category`, `date`) VALUES ('TCL', 118, 'Auto && Transport', '2021-06-01');


